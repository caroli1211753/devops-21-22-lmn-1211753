#Relatório CA1

**Nome:** Carolina Silva

**Número:** 1211753

---
#### Parte 1
**1. Criar nova pasta CA1**

Na linha de comando usar o comando **mkdir CA1** e depois copiar a pasta com o código para dentro da pasta.

**2. Fazer Commit das alterações**

Ao alterarmos o nosso ficheiro teremos de enviar as actualizações para o nosso repositório remoto. Para isso utilizamos os seguintes comandos:

* **git status** para analisar se há algum ficheiro alterado localmente e que seja para ser analisado sem ter sido feito commit (neste momento temos 2 ficheiros). Os ficheiros que não devem ser analisados deverão ser inseridos no ficheiro .gitignore.
* **git add .** para adicionar todos os ficheiros alterados e que são para ser analisados à staging area. Com o comando **git add <file>** premite seleccionar o ficheiro que queremos colocar na staging area (onde <file> é o nome do ficheiro a enviar).
* **git commit** permite enviar os ficheiros que estão na staging area para a pasta .git que depois permite enviar para o repositório com o comando seguinte.
* **git pull** permite baixar as alterações que estão no repositório para o nosso projecto remoto.
* **git push** envia as alterações/ficheiros assinalados nos comandos anteriores para o repositório.

**3. Tags**

* **git tag <tagCode>** cria uma etiqueta com o nome tagCode e aplica ao último commit. Podemos usar o comando **git tag -a <tagName> <checksum>** permite colocar a etiqueta tagName num commit específico (como checksum indicado). O uso do -a permite que a tag tenha anotações, como por exemplo, quem cria a tag. O uso de -m permite a criação de uma mensagem para a etiqueta.
* **git log** para podermos consultar a lista de commits efectuados. Para facilitar a análise utilizamos **git log --pretty=oneline** para aparecer um resumo da lista numa linha e ser mais fácil analisar e escolher o código checksum do commit. 
* O comando **git show <tagName>** mostra a etiqueta tagName: Mostra o comentário colocado, o commit ao qual faz parte e as diferenças deste commit para o anterior.
* **git tag -n** permite consultar a lista de etiquetas e os seus comentários. Os comentários permitem identificar/explicar mais explicitamente o que é aquela etiqueta.
* **git push origin --tags** permite enviar as etiquetas criadas para o repositório remoto/comum. **git push origin <tagName>** permite enviar para o repositório remoto especificamente a etiqueta *tagName*.
* **git tag -d** permite apagar uma tag localmente. Para apagar uma etiqueta específica (tagName) no repositório remoto utilizar o comando **git push --delete origin <tagName>**  

#### Parte 2
**1. Criar novo branch**

* **git branch <branchName>** para criar um novo branch com nome branchName.
* **git branch** permite consultar qual o branch que estamos a trabalhar.
* **git checkout <branchName>** para mudar de ramo. Isto muda o apontador HEAD para o branch que escolhemos (branchName). Para guardarmos as alterações e antes de mudar de ramos devemos usar os comandos **git add .** e **git commit -a -m "message"** para guardar as alterações.
* **git log --oneline --decorate --graph --all** permite uma lista dos commits e quais os branch que existem.
* Quando acabarmos as alterações no branch em que estamos e queremos mudar/transferir estas alterações para o branch master, depois de as guardar na staging area, devemos mudar para o branch master (fazendo **git checkout master**) e depois fazer o **git merge <branchName>** para conseguirmos resolver os conflitos. Depois de verificar se existem conflitos estes devem ser resolvidos num editor de texto indicando o que se deve manter. As partes que têm *>>>>*, *<<<<* e *====* devem ser eliminadas e depois deve manter-se a informação que se pretende.
* Para enviar o branch para o repositório remoto devemos usar comando **git push origin <branchName>**.


#### Alternativa

Para alternativa ao git para conseguir obter controlo de versões utilizei o *Mercurial*. O repositório remoto ficou no *SorceForge* no seguinte link https://sourceforge.net/p/devops-21-22-lmn-1211753/code/ci/default/tree/.

Os comandos básicos para efectuar o clone do repositório e fazer add, commit e push são bastante semelhantes:
* **hg clone <repositoryAddress>** permite fazer o clone;
* **hg add** permite adicionar ficheiros alterados;
* **hg commit** permite fazer commit dos ficheiros adicionados e coloca-los na staging area. O comentário no meu caso foi efectuado ao abrir o editor de texto (notepad no meu caso);
* **hg push** permite enviar para o repositório remoto as alterações;
* **hg pull** permite tazer as alterações que temos no respositório remoto;
* **hg log** permite consultar a lista de commits efectuados. No log aparece o *revision number* é uma ordem pelo qual os commits foram efectuados (apenas é válida para aquele repositório especifico), *changeset* que é o identificador do commit (equivalente ao checksum do git), utilizador que efectuou o commit, data em que foi efectuado e a mensagem acopulada ao commit.
* **hg tag <tagName>** cria uma etiqueta e aplica ao último commit
* **hg branch <branchName>** permite criar um branch
* **hg update <branchName>** pemrite alternar entre branch
* **hg push --new-branch** permite adicionar o branch ao repositório

####Comparação das duas alternativas
O git é um sistema de controlo de versões que permite o controlo/gestão de actualizações realizadas no projecto de desenvolvimento de software. Este tipo de ferramenta, junto com o repositório, é principalmente importante quando o desenvolvimento é efectuado em equipas pois guarda as alterações efectuadas num conjunto de ficheiros ao longo do tempo. Isto permite sempre voltar para uma versão anterior sempre que necessário. Mas também que cada um dos membros da equipa tenha acesso à ultima versão do projecto.
O git funciona com o sistema de snapshots das alterações efectuadas e com apontadores para a versão anterior. Cada versão tem um código identificador unitário (40 caracteres) chamado checksum.
Permite a criação de tags para marcar versões. Também permite alguma flexibilidade de criação de branch para permitir a correcção de erros sem danificar o ramo master. Depois de termos as alterações testadas e validadas é possível fazer um merge com o ramo master. Por defeito o git efectua um fast merge e tenta interpretar as diferenças. No caso de não conseguir será apresentado um ficheiro que permite avaliar o merge necessário.

A alternativa estudada foi Mercurial. Funciona com sistema de changeset que agrupa commits.
Algumas diferença que consegui identificar é que o git possui mais comandos e os utilizadores devem conhece-lo antes de usar porque podem causar problemas na aplicação, no entanto ele é mais flexível. O Mercurial apresenta menos comandos o que não permite ter tanta flexibilidade como o git. Ou seja, Git será melhor para utilizadores mais experientes enquanto que o Mercurial será melhor para novatos.
Em termos da utilização de banch o Git a forma de funcionamento é mais simples do que o Mercurial.
O Mercurial não faz o merge por ele próprio nem tenta resolver os conflitos ao contrário do git.

**Nota:** Ao desenvolver o projecto com o git consegui efectuar todos os exercícios propostos mas o meu código estava a falhar por não ter colocado as dependências correctas. No último commit fiz um novo branch e resolvi o problema mas tive de refazer as alterações.
