
# Relatório CA2 - gradle

**Nome:** Carolina Silva

**Número:** 1211753

---
#### Parte 1

Gradle é uma ferramenta que automatiza a compilação de projectos de várias linguagens. Controla o processo de desenvolvimento de software com base em tarefas (tasks).  Suporta as seguintes linguagens:
* Java (as well as Kotlin, Groovy, Scala);
* C/C++;
* JavaScript

O Gradle é uma evolução de Apache Maven (que, por sua vez é uma evolução do Apache Ant) mas que usa um ficheiro de configuração do projecto escrito com Groovy ao contrário do anterior que usava XML. A linguagem Groovy é mais intuitiva e permite mais flexibilidade de configuração. O Maven usa uma convenção de organização do código do projecto (em termos de directórios - exemplo, src-->java-->...), essa convenção também foi passada para o Gradle, no entanto, pode ser facilmente contornada se assim for a vontade o desenvolvedor de software. O Gradle funciona no Java Virtual Machine (máquina virtual que permite correr programas em java mas também com outras linguagens que são compiladas em Java bytecode.
Foi desenhado para projectos com diversas compliações e que podem ser grandes. Funciona com base numa série de tarefas de compilação que podem correr com uma ordem ou em paralelo. Também suporta compilações incrementais , se forem determinadas partes que já estejam actualizadas e não necessitem de nova compilação.

Para conseguirmos configurar a compilação de um projecto com gradle apenas editamos o ficheiro build.gradle. Neste ficheiro temos tarefas, dependencias,...

É possível utilizar o Gradle num projecto sem que se tenha instalado o plug-in do Gradle. Isto se o projecto estiver preparado e tenha o ficheiro gradlew. Se for instalado o plug-in, na linha de comandos podemos usar o comando **gradle <accção a realizar>** se este não estiver instalado o comando a usar será **gradlew <accção a realizar>**.


##### 3. Criar uma tarefa para correr o servidor
No ficheiro build.gradle adicionar a seguinte tarefa:

    task runServer(type:JavaExec, dependsOn: classes){
    group = "DevOps"
    description = "Launches a server on port 59001 "

        classpath = sourceSets.main.runtimeClasspath

        mainClass = 'basic_demo.ChatServerApp'

        args '59001'
    }

As características definidas para esta tarefa foram as seguintes:

* **Type** *JavaExec* pois é uma tarefa que se pretende executar 

* **dependsOn** *classes* é necessário ter as classes compiladas para se poder usar o código que está dentro destas para realizar a tarefa pretendida.

* **group** *DevOps* para tarefas criadas no âmbito da aula, isto permite organização na visualização das tasks quendo se executa o comando **gradlew tasks**.

* **classpath** *sourceSets.main.runtimeClasspath*: importa definir para ser independente da máquina em que se usa/instala a aplicação.
  
  * *sourceSets* representa a organização das pastas e ficheiros java. Esta pode ser definida no build.gradle, neste caso, está definido como default seguir as convenções do Maven(src, main, java,...).
  
  * *runtimeClasspath* é usado quando executamos as classes desta sourceSet.

* **main class** é o caminho até à classe que se pretende utilizar na tarefa.

* **args** são os argumentos utilizados na classe indicada anteriormente e que permite correr a tarefa.

Para corrermos a tarefa podemos utilizar na command line o seguinte comando **gradlew <taskName>** 

Além das tarefas que podemos definir o projecto já tem as suas tarefas que foram definidas pelo programador que criou a aplicação ou são adquiridas dos plugins indicados para a aplicação.
Podemos consultar as tarefas existentes com o comando **gradlew <tasks>**

No caso desta aplicação já temos tarefas pré-definidas para os seguintes grupos:
* BuildSetup tasks
* DevOps tasks
* Distribution Tasks
* Documentation Tasks
* Help tasks

O comando **gradlew tasks --all** permite ver todas as tarefas incluindo aquelas que não estão inseridas em nenhum grupo.

A criação desta tarefa facilita/automatiza a tarefa pois não é necessário usar o comando indicado inicialmente:
*% java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp <server port>*

##### 4. Acrescentar teste e dependências necessárias
Para criar o teste temos de seguir a convenção que já temos implementada, ou seja, é necessário no directório main, criar a pasta *test*, dentro a pasta **java**, dentro a pasta **basic_demo**. Isto à semelhança de como já está o caminho para as classes da aplicação.
Depois de criar este caminho criamos ficheiro "AppTest.java" e criamos o teste.

Para funcionar temos de adicionar dependências no ficheiro build.gradle. Vamos então adicionar ao grupo/separador *dependencies* a seguinte dependência **testImplementation "junit:junit:4.12"** :

    dependencies {
    // Use Apache Log4J for logging
    implementation group: 'org.apache.logging.log4j', name: 'log4j-api', version: '2.11.2'
    implementation group: 'org.apache.logging.log4j', name: 'log4j-core', version: '2.11.2'
    testImplementation "junit:junit:4.12"
    }

* **

Para testar se o teste criado está a funcionar podemos usar o comando **gradlew test**

##### 5. Criar tarefa para copiar ficheiros e fazer backup da src
Acrescentar a seguinte tarefa no ficheiro build.gradle:

    task backUp(type: Copy){
      from 'src'
      into 'backup'
    }

Neste caso a tarefa deverá ser type Copy para copiar da src para uma pasta backup.

Para testar a execução da tarefa usar o comando **gradlew backUp**

##### 6. Criar tarefa para criar um ficheiro zip para guardar src da aplicação

Acrescentar a seguinte tarefa no ficheiro build.gradle:

    task zip (type: Zip){
      from 'src'
      archiveName 'zip_1.test'
      destinationDir(file('build'))
    }

Para testar a execução da tarefa usar o comando **gradlew zip**

-----
#### Part2

##### 1. Criar branch tut-basic-gradle and use for this part
* **git branch tut-basic-gradle** para criar o branch

* **git checkout tut-basic-gradle** para mudar para o branch novo e começar o desenvolvimento de novas actualizações nesse branch.

Pode e deve fazer-se commits com actualizações importantes, ou seja, usando os comandos **git add .** e **git commit**

##### 2. Iniciar um novo project gradle spring boot 
##### 3. Extrair o zip e abrir o projecto. Analisar as tasks existentes
Com o comando **gradlew tasks**

Apresenta as seguintes tasks:

    Application tasks
    -----------------
    bootRun - Runs this project as a Spring Boot application.

    Build tasks
    -----------
    assemble - Assembles the outputs of this project.
    bootBuildImage - Builds an OCI image of the application using the output of the bootJar task
    bootJar - Assembles an executable jar archive containing the main classes and their dependencies.
    bootJarMainClassName - Resolves the name of the application's main class for the bootJar task.
    bootRunMainClassName - Resolves the name of the application's main class for the bootRun task.
    build - Assembles and tests this project.
    buildDependents - Assembles and tests this project and all projects that depend on it.
    buildNeeded - Assembles and tests this project and all projects it depends on.
    classes - Assembles main classes.
    clean - Deletes the build directory.
    jar - Assembles a jar archive containing the main classes.
    testClasses - Assembles test classes.

    Build Setup tasks
    -----------------
    init - Initializes a new Gradle build.
    wrapper - Generates Gradle wrapper files.
    
    Documentation tasks
    -------------------
    javadoc - Generates Javadoc API documentation for the main source code.
    
    Help tasks
    ----------
    buildEnvironment - Displays all buildscript dependencies declared in root project 'react-and-spring-data-rest-basic'.
    dependencies - Displays all dependencies declared in root project 'react-and-spring-data-rest-basic'.
    dependencyInsight - Displays the insight into a specific dependency in root project 'react-and-spring-data-rest-basic'.
    dependencyManagement - Displays the dependency management declared in root project 'react-and-spring-data-rest-basic'.
    help - Displays a help message.
    javaToolchains - Displays the detected java toolchains.
    outgoingVariants - Displays the outgoing variants of root project 'react-and-spring-data-rest-basic'.
    projects - Displays the sub-projects of root project 'react-and-spring-data-rest-basic'.
    properties - Displays the properties of root project 'react-and-spring-data-rest-basic'.
    tasks - Displays the tasks runnable from root project 'react-and-spring-data-rest-basic'.
    
    Verification tasks
    ------------------
    check - Runs all checks.
    test - Runs the test suite.
    
    Rules
    -----
    Pattern: clean<TaskName>: Cleans the output files of a task.
    Pattern: build<ConfigurationName>: Assembles the artifacts of a configuration.

Existem tarefas pré-definidas que foram criadas tendo em conta os plugins e as configurações escolhidas para a criação do projecto.

##### 5. Extrair o zip e abrir o projecto. Analisar as tasks existentes
Copiada a pasta src e também webpack.config.js e package.json.
Apagada a pasta  src/main/resources/static/built/

##### 6. Correr a comando bootRun e confirmar se a nossa aplicação está a funcionar (visitar a porta 8080)



##### 8. Adicionar plugin dependendo da versão escolhida na criação do projecto

Neste caso acrescentar aos plugins *id "org.siouan.frontend-jdk11" version "6.0.0"*

    plugins {
    id 'org.springframework.boot' version '2.6.6'
    id 'io.spring.dependency-management' version '1.0.11.RELEASE'
    id 'java'
    id "org.siouan.frontend-jdk11" version "6.0.0"
    }

##### 9. Configurar o plugin inserido

Acrescentar no ficheiro build.gradle:

    frontend {
    nodeVersion = "14.17.3"
    assembleScript = "run build"
    cleanScript = "run clean"
    checkScript = "run check"
    }

##### 10. Actualizar os scripts

Actualizar no ficheiro build.gradle:

    "scripts": {
    "webpack": "webpack",
    "build": "npm run webpack",
    "check": "echo Checking frontend",
    "clean": "echo Cleaning frontend",
    "lint": "echo Linting frontend",
    "test": "echo Testing frontend"
    },  

##### 11. Tentar executar o build

Utilizar o comando **gradlew build**
No caso foi necessário realizar a tarefa **gradlew clean** pois o build anterior tinha usado a plugin com configurações erradas. Depois deste comando com **gradlew build** funciona correctamente.

Com **CTRL+C** podemos terminar o trabalho (job).

Ao utilizar o comando **gradlew build --info** conseguimos ver mais informações enquanto é executada a tarefa de build.



##### 12. Executar bootRun

Neste caso, se formos consultar a porta 8080 confirmamos que já temos a app a funcionar.

##### 13. Adicionar uma tarefa para copiar o ficheiro jar gerado para o directório "dist"

    task copyJar(type: Copy, dependsOn: jar){
    group = "DevOps"
    description = "Copy file jar to a new directory dist"
    
        from 'build/libs/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT.jar'
        into 'dist'
    }

##### 14. Adicionar uma tarefa para eliminar todos os files gerados pelo webpack

[comment]: <> (clean.doFirst &#40;&#41; Acontece quando se chama clean faz a tareda que está dentro do first.)

[comment]: <> (não é isto que queremos.)

    task deleteWebpack(type: Delete, dependsOn: build){
    group = "DevOps"
    description = "Delete files generated webpack"
            delete 'build/resources/main/static/built/bundle.js' //caminho reativo desde o build.gradlew
        }

    clean.dependsOn deleteWebpack

##### 15. Adicionar uma tarefa para eliminar todos os files gerados pelo webpack

Depois de fazer **git add .** e **git commit** no ramo/banch em que estamos a trabalhar utilizar o comando **git checkout master** para voltar ao ramo master. No ramo master confirmar, com **git status** se tudo o que temos já se encontra na staging area. Se sim, colocar o comando **git merge tut-basic-gradle** para fazer o merge das alterações. Depois disto resolver os conflitos e guardar. No final podemos fazer **git push** para enviar para o repositório remota.

##### 16. Actualizar o ReadMe

##### 17. Criar etiqueta "ca2-part2" e associar ao último commit
**git tag ca2-part2** para criar a etiqueta. Depois colocar **git push origin tut-basic-gradle** para enviar a etiqueta para o repositório remoto.

---
##Alternativa

A alternativa estudada foi o Maven de onde o Gradle nasceu.

O Maven apresenta as seguitntes características:
* Apresenta sistema de dependências;
* Permite compilações de multiplos módulos (permite apenas fazer build de "novidades" do projecto mantendo de builds anteriores o que não foi alterado);
* Estrutura de projectos consistente;
* Consistência na complilação do modelo;
* Orientado a plugin.

Todos as compilações/build são essencialmente a mesma sequência:
1. Compilação do código;
2. Cópia da Resource;
3. Compilação de testes e testar;
4. "Empacotar" o projecto;
5. Enviar o projecto para repositório remoto;
6. Limpeza;
7. Descrição do projecto e configuração de compilação;

Não é possível alterar o build e Maven não tem conceito de condição. Os plugins são possíveis de configurar.

O ficheiro de configuração é o POM (Project Object Model) e utiliza a extensão .xml. Este ficheir descreve o projecto, nome e versão. Também descreve o tipo de artefacto, a localização do cósigo fonte, dependências, plugins.

O Maven indentifica o projecto unicamente utilizando:
* groupID: identifica o grupo (sem espaçõs ou virgulas)
Maven uniquely identifies a project using:
groupID: Arbitrary project grouping identifier (no spaces or colons)
Usually loosely based on Java package
artfiactId: Arbitrary name of project (no spaces or colons)
version: Version of project
Format {Major}.{Minor}.{Maintanence}
Add ‘-SNAPSHOT‘ to identify in development
GAV Syntax: groupId:artifactId:version

O Maven cria automaticamente um ciclo de vida:

* validate: valida as informações necessárias do projecto;
* compile: compila o código src;
* test: testa o código compilado;
* package: agrupa o código compilado;
* integrationtest: testa o código compilado com testes de integração;
* verify: verifica o código com critério definido;
* install: instala o package num repositório local;
* deploy: implanta código num repositório remoto.

Nota: A implementação de alternativa está demonstrada num pasta adicional: Tentei implenetar e percebi que era possível configurar plugin mas não dará para criar/programar as tarefas.





