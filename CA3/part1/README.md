
# Relatório CA3 Parte 1 - Virtual Machine

**Nome:** Carolina Silva

**Número:** 1211753

---
#### Parte 1 - Virtualização

O processo de virtualização consiste num software de ambiente computacional que executa programas como um computador real.
Um máquina virtual (VM) pode ser definida como uma duplicação isolada da máquina real, isto é, um máquina virtual criada à semelhança da máquina real (em termos de hardware) mas é totalmente protegida. O sistema operativo da máquina virtual poderá ser completamente diferente do da máquina real.
A máquina virtual é implementada por um Hypervisor (por exemplo, Oracle VM VirtualBox, VMware, Microsoft Hyper-V, entre outros). Este permite definir o conjunto de hardware disponível para máquina virtual. Este conjunto mais o sistema operativo formam a máquina virtual. Um Hypervisor pode criar multiplas máquinas virtuais. O hypervisor pode ou não correr no sistema operativo. 

Há dois tipos de virtualização:
* Virtualização ao nível do hardware: o hypervisor corre directamente no hardware da máquina real (host);

* Virtualização hospedada: o hypervisor corre no sistema operativo da máquina real.

Dependendo do tipo de virtualização as máquinas virtuais (guests) podem ter mais ou menos conhecimento se estão a correr num hardware virtual. Com VirtualBox, que vamos usar para criar máquina virtual o sistema operativo pensa que está a correr numa máquina real.

Algumas exemplos de razões para a utilização da virtualização são: 

* Testar a aplicação em diferentes sistemas operativos para analisar o comportamento;

* Partilhar máquinas virtuais com colegas;

* Criar soluções para correr na Cloud.

**Ligação entre Cloud e Virtualização**

Virtualização apareceu antes da Cloud. Serviços na cloud precisam de virtualização. 
Ao usar um destes serviços, vamos ter de criar conta e depois definir que tipo de máquina quero usar:

* Tipo de servidor;

* Tamanho (processadores, memória RAM, ROM);

* Software necessário;

* Tipo de base de dados.

Que são passos da virtualização.

Mais tarde apareceram os containers, por exemplo Docker, baseado em virtualização e também usado na Cloud.

#### 1. Criar a máquina virtual (VM)
Com o hypervisor VirtualBox criamos uma máquina virtual. Para isso definimos várias informações:

* Nome;

* Onde ficará guardada na máquina real; 

* De que tipo será (sistema operativo); 

* Quantidade de memória RAM; 

* Quantidade de memória no disco além do tipo de ficheiro do disco (se não for para partilhar com outro software de virtualização pode deixar-se o default). As opções disponibilizadas são as seguintes:
  
  * **VDI** que é o formato do disco virtual no Oracle VirtualBox; 
  
  * **VHD** é o formato do disco virtual produtos de virtualização da Microsoft's;
  
  * **VMDK** é o formato do disco virtual VMware's.
   Aqui também se define a alocação:
  
  * dinâmica: é alocado à máquina virtual assim que é utilizado, até ao tamanho definido como máximo;
  
  * fixa: fica logo alocado à máquina virtual o espaço definido como máximo para a memória. Demora mais a criar mas é normalmente mais rápida.

Depois de definirmos e criarmos a máquina que pretendemos temos de lhe instalar o sistema operativo pretendido. Esta instalação é feita através de um CD virtual/imagem de um CD (.iso) que é inserido na drive virtual da máquina virtual.
Na instalação do software definimos algumas características:

* configuração do teclado;

* arquivo onde vamos buscar os resources e actualizações (aplicações para ubuntu);

* definir se o acesso à internet passa por proxy (neste caso não);

* time zone.

Além destas configurações, já temos uma ligação NAT que nos permite ligar à internet mas teremos de colocar/adicionar e configurar uma placa (tipo host only adaptor) para do computador real à máquina virtual. Aqui teremos de configurar o IP deste adaptador para um disponível na rede em que está inserido.
Vamos permitir DHCP (protocolo de configuração dinâmica de host).

Para instalar as configurações necessárias em termos de software e aplicações a usar vamos usar a sessão administrador **sudo**.
Assim, alguns comandos necessários:

* **sudo apt update**: para actualizar todos os pacotes possiveis de instalar; 

* **sudo apt install net-tools**: pacote de ferramentas que permitem fazer a gestão de rede;

* **sudo nano /etc/netplan/01-netcfg.yaml** aceder ao ficheiro de configuração (utilizando o nano que é um programa para editar ficheiros de texto. Aqui usa-se Ctrl+O para guardar e Ctrl+X para sair);

* **sudo netplan apply**: depois de alterar temos de aplicar as alterações;

* **sudo apt install openssh-server**: instalar ssh que permite aceder à maquina virtual pela máquina rela numa sessão segura;

* **sudo nano /etc/ssh/sshd_config**: para aceder ao ficheiro de configuração e configurar para usar a pasword como autenticação e depois usar o comando **sudo service ssh restart** para re iniciar ssh e aplicar as alterações nas configurações.

* **sudo apt install vsftpd** para instalar FTP (file transfer protocol) para transferir ficheiros de/para a máquina virtual de máquinas reais. Aqui teremos de ir ao ficheiro de configurações mudar para permitir a escrita e depois fazer o restart do recurso:
  
  * **sudo nano /etc/vsftpd.conf** e descomentar a linha write_enable=YES 
  
  * **sudo service vsftpd restart**

Agora é possível aceder à máquina virtual através da linha de comandos da máquina real com o comando **ssh <nome Da Maquina Virtual>@<IP da máquina virtual>**. Nota: A máquina virtual tem de estar ligada para ser possível acede-la.;

Para acabar é necessário instalar git como o comando **sudo apt install git** e o java **sudo apt install  openjdk-8-jdk-headless**

#### 2. Clonar repositório para dentro da máquina virtual

Na linha de comandos da máquina virtual ou na linha de comandos da máquina real se a sessão estiver aberta na máquina virtual usar o comando **git clone <endereço do repositório>**

Para sair da sessão ssh usar o comando **exit**

#### 3. Compilar e executar o projecto spring boot tutorial basic e gradle_basic_demo
##### spring boot tutorial basic
Para ser possível correr o springBoot com o comando **./mvnw spring-boot:run** é necessário mudar as permissões no ficheiro mvnw com o comando **chmod +x nome do ficheiro** neste caso nome do ficheiro é mvnw.

##### gradle_basic_demo
Para ser possível fazer a compilação do projecto com o comando **./gradlew build** é necessário mudar as permissões no ficheiro gradlew como comando **chmod +x nome do ficheiro>** neste caso nome do ficheiro é gradlew.

#### 4.Para aceder aos projectos web devemos usar a máquina real
Este é o caso da aplicação tutorial basic usa-se o endereço no browser da máquina real **http://192.168.56.5:8080/** onde 192.168.56.5 é o IP da nossa máquina virtual onde está a correr a aplicação.

#### 5. Para projectos como a aplicação de chat deve executar-se o servidor na máquina virtual e os clientes na máquina real
No caso da aplicação de chat foi possível fazer correr/lançar o servidor mastivemos algumas dificuldades com a versão do gradlew que estavamos a utilizar, ou seja, tivemos de limpar o build que já tinhamos (que tinha uma versão do gradle) e fazer o build de novo.
Neste caso usando o comando **./gradlew runServer** foi possível iniciar o servidor na máquina virtual

![img.png](img.png)

Neste caso para correr os cliente deve usar-se a máquina real pois esta aplicação tem interface gráfica e a máquina virtual não tem interface gráfica. Para se poder correr na máquina real o cliente foi necessário proceder à alteração da tarefa runClient.
Aqui alterou-se o endereço do localhost para o IP da máquina onde está a correr o servidor. Como demonstrado em baixo;

    task runClient(type:JavaExec, dependsOn: classes){
    group = "DevOps"
    description = "Launches a chat client that connects to a server on VM 192.168,56,5:59001 "
    
        classpath = sourceSets.main.runtimeClasspath
    
        mainClass = 'basic_demo.ChatClientApp'
    
        args '192.168.56.5', '59001'
    }

Foi possível correr a task:
![img_1.png](img_1.png)

E abriu a interface grafica e o servidor também percebeu que havia um novo utilizador (imagem abaico).
![img_2.png](img_2.png)

Nota: Acrescentei o gradle demo neste directório para conseguir testar numa versão limpa da aplicação.