
# Relatório CA4 Parte 1 - Docker

**Nome:** Carolina Silva

**Número:** 1211753

---
## Parte 1 - Containers - Docker


Docker é uma ferramenta open source que permite a criação e manutenção de containers (semelhante ao que o Vagrant faz com máquinas virtuais). 

Importa perceber diferenças entre containers e máquinas virtuais:

**Containers:**

* mais leves, no máximo contêm a aplicação e os arquivos necessários para a executar;

* normalmente usados para empacotar funções individuais que realizam tarefas específicas (microserviços).

* funcionam em cima do sistema operativo da máquina host;

* fáceis de migrar entre ambientes.

**Máquinas Virtuais:**

* funcionam em cima do hypervisor (cujo funciona em cima do sistema operativo da máquina host) mas contém o seu próprio sistema operativo;

* mais pesadas, têm o seu próprio sistema operativo, o que possibilita a execução simultânea de várias funções com uso intenso de recursos;

* por terem maior número de recursos à disposição, são capazes de abstrair, dividir, duplicar e emular os servidores por inteiro (sistema operativo, ambiente de trabalho, base de dados e redes).


O Docker cria containers especificados no Dockerfile. Neste Dockerfile podemos inicar a configuração do container com base em imagens já existentes na rede (https://hub.docker.com/search?q=images).

Uma imagem de container é um pacote executável de software que inclui tudo o que é necessário para correr a aplicação (código, tempo de execução, ferramentas do sistema, bibliotecas e configurações de sistema) - uma imagem é criada com o comando **docker build -t *ContainerName* .**. Por outro lado, um container está a correr uma instancia de uma imagem - um contentor pode ser colocado a correr com o comando **docker run -p *hostMachinePort*:*containerPort* -d <ContainerName>**.

As imagens são o pacote análogo ao código ou programa enquanto os containers são a parte executável, análogo ao processo. 


### Part1a - In this version you should build the chat server "inside" the Dockerfile

Na pasta referente a esta parte (onde queremos criar o container) teremos de criar um ficheiro Dockerfile. Este é um ficheiro de texto que contém todos os comandos que o utilizador pode usar na linha de comandos para construir uma imagem. Utilizando o comando **docker build** podemos criar um build automático que executa os comandos descritos no ficheiro Dockerfile e cria a imagem.
O container é criado por layers, normalmente inicia-se com uma imagem já existente e por cima disso, em layers vamos adicionado pacotes, a aplicação, portas expostas pra a máquina host e o comado que se pretende executar. Se não tivermos a linha CMD o container é criado e desliga logo.
O Dockerfile deve conter os seguintes comandos com a explicação de cada um deles:

    FROM ubuntu:18.04 ==> imagem de onde se pretende partir para a criação do container
    
    RUN apt-get update -y ==> comando para instação e actualização de pacotes (-y para confirmação)
    RUN apt-get install openjdk-11-jdk-headless -y ==> comando para instação do java, neste caso jdk11
    RUN apt-get install git -y ==> instalação do git
    RUN git clone https://bitbucket.org/luisnogueira/gradle_basic_demo/ ==> fazer clone da aplicação
    
    WORKDIR gradle-basic-demo/ ==> definição do caminho para directório de trabalho
    RUN chmod u+x gradlew ==> alteração de autorizações para o ficheiro gradlew
    RUN ./gradlew clean build ==> execução de tarefa gradle clean build
    
    EXPOSE 59001 ==> definição da porta do container que permite comunicação
    
    CMD java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001 ==> comando para executar o servidor desta aplicação

Depois da definição do docker file utilizar o comando:

**docker build -t *ContainerName* .** para ler os comandos indicados nos dockerfile e criação da imagem definida.

Se não estiver certo ao início podemos utilizar o comando **docker build --no-cache -t *ContainerName* .** para não usar o que está em cache (de eventuais build que tenhamos feito com este dockerfile)

Depois de fazermos build como comando **docker run -p *hostMachinePort*:*containerPort* -d <ContainerName>** (no nosso caso, **docker run -p 59001:59001 -d ca41a**). 
* -p para indicar as portas expostas host:container;
* -d detatch, ou seja, para correr no background.

Com o comando **docker images** podemos ver as imagens que temos na máquina host.
    
**docker ps** permite analizar quais os containers que estão em funcionamento na máquina host

Fazer tag da imagem e publicar no Docker Hub:

    docker login
    docker tag ca41a caroli1211753/ca4part1_a:v1.0
    docker push caroli1211753/ca4part1_a:v1.0

Importante que a tag seja: *userName*/*nameImage*:*version*

Tag publicada em: https://hub.docker.com/repository/docker/caroli1211753/ca4part1_a/general

Mais alguns comandos do Docker utilizados:

**docker rm *ContainerId*** ==> eliminar o container

**docker container prune** ==> elimina todos os containers

**docker stop *ContainerId*** ==> pára o container


### Part1b -  In this version you should build the chat server in your host computer and copy the jar file "into" the Dockerfile

O Dockerfile deve conter os seguintes comandos com a explicação de cada um deles:

    FROM ubuntu:18.04 ==> imagem de onde se pretende partir para criação de container

    RUN apt-get update -y ==> comando para instação e actualização de pacotes (-y para confirmação)
    RUN apt-get install openjdk-11-jdk-headless -y ==> comando para instação do java, neste caso jdk11
    
    COPY basic_demo-0.1.0.jar . ==> comando para copiar o ficheiro (que se encontra na pasta do dockerfile para a raiz do container
    
    EXPOSE 59001 ==> definição da porta do container que permite comunicação
    
    CMD java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001 ==> comando para executar o servidor desta aplicação

Neste caso não vamos ter de instalar alguns pacotes instalados anteriormente porque vamos instalar a aplicação através do ficheiro .jar

Também para conseguir que funcionasse, porque tinha na máquina host (onde a aplicação tinha feito build) o java versão 17 tive de colocar no build gradle o seguinte para forçar a compilação com java versão 11:

    java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(11))
    }
    }

Na máquina host necessário alterar a seguinte task no documento build.gradle para funcionar localhost (no outro exercício tinhamos definido para o IP da máquina onde queríamos correr)

    task runClient(type:JavaExec, dependsOn: classes){
    group = "DevOps"
    description = "Launches a chat client that connects to a server on localhost:59001 "
    
        classpath = sourceSets.main.runtimeClasspath
    
        mainClass = 'basic_demo.ChatClientApp'
    
    args 'localhost', '59001'
    }

Depois da definição do docker file utilizar o comando:

**docker build -t <ContainerName> .** para ler os comandos indicados nos dockerfile e criação da imagem definida.

Se não estiver certo ao início podemos utilizar o comando **docker build --no-cache -t <ContainerName> .** para não usar o que está em cache (de eventuais build que tenhamos feito com este dockerfile)

Depois de fazermos build como comando **docker run -p *hostMachinePort*:*containerPort* -d <ContainerName>** (no nosso caso, **docker run -p 59001:59001 -d ca41a**).
* -p para indicar as portas expostas host:container;
* -d detatch, ou seja, para correr no background.

Com o comando **docker images** podemos ver as imagens que temos na máquina host.

**docker ps** permite analizar quais os containers que estão em funcionamento na máquina host

Fazer tag da imagem e publicar no Docker Hub:

    docker login
    docker tag ca41a caroli1211753/ca4part1_a:v1.0
    docker push caroli1211753/ca4part1_a:v1.0

Importante que a tag seja: *userName*/*nameImage*:*version*

Tag publicada em: https://hub.docker.com/repository/docker/caroli1211753/ca4part1_b


### Part2 - Docker - Docker-Compose

Compose é uma ferramenta do Docker para definir e correr uma aplicação multi-container com Docker. Com esta ferramenta usa-se um ficheiro docker-compose.yml para configurar os serviços necessários para aplicação. Depois com o comando **docker compose up** pode-se criar e inicar todos os serviços definidos no ficheiro. 
Usar o compose consiste em 3 passos:

* definir o ambiente(s) da aplicação com Dockerfile, por cada container temos um Dockerfile;

* definir os serviços que fazem funcionar a aplicação no ficheiro docker-compose.yml para poderem correr juntos num ambiente isolado;

* Usar o comando **docker compose up** para criar e iniciar os containers definidos para a aplicação (também se pode usar **docker-compose up**).

O nosso ficheiro docker-compose é o seguinte (indicada explicação do conteúdo):

    version: '3'
    services:
     web: ==> definição do container web
      build: web
      ports:
        - "8080:8080" ==> portas de comunicação porta exposta do container : porta que "ouve" do host
      networks: ==> definição da rede interna
       default:
        ipv4_address: 192.168.56.10 ==> definição do IP do container web
      depends_on: ==> depende da criaçãod a db, ou seja, db vai ser criada primeiro.
        - "db"
     db: ==> definição do container db
      build: db
      ports:
         - "8082:8082" ==> definição das portas de comunicação
         - "9092:9092"
      volumes: ==> definição da pasta partilhada entre máquina host e container
         - ./data:/usr/src/data-backup ==> a pasta na root "data" da máquina host comunica com a pasta "data-backup" que se encontra dentro dos directorios usr/src
      networks:
        default:
         ipv4_address: 192.168.56.11 ==> definição do IP do container db
    networks:
       default:
         ipam:
           driver: default
           config:
             - subnet: 192.168.56.0/24 ==> configuração da rede interna de comunucação dos containers.


O Docker cria uma rede (bridge network) para a comunicação entre containers. No caso do ficheiro docker-compose anterior nós definimos uma rede interna mas poderíamos analisar qual a rede criada pelo Docker e utilizar essa.

Além do ficheiro docker-compose é necessário haver um Dockerfile para cada container a criar. No nosso caso:

**Dockerfile para imagem/container db**

    FROM ubuntu ==> imagem a partir da qual é criado
    
    RUN apt-get update && \
    apt-get install -y openjdk-8-jdk-headless && \
    apt-get install unzip -y && \
    apt-get install wget -y ==> comandos para instalação em camadas dos packages necessários à aplicação
    
    RUN mkdir -p /usr/src/app ==> comando para criar o seguinte directório /usr/src/app
    
    WORKDIR /usr/src/app/ ==> alteração do directório de trabalho para usr/src/app
    
    RUN wget https://repo1.maven.org/maven2/com/h2database/h2/1.4.200/h2-1.4.200.jar ==> no directório anterior correr o ficherio jar indicado
    
    EXPOSE 8082 ==> portas expostas para comunicação com máquina host
    EXPOSE 9092 ==> portas expostas para comunicação com máquina host
    
    CMD java -cp ./h2-1.4.200.jar org.h2.tools.Server -web -webAllowOthers -tcp -tcpAllowOthers -ifNotExists ==> correr o comando para colocar o Servidor a correr

**Dockerfile para imagem/container web**

    FROM ubuntu:18.04 
    RUN apt-get -y update && apt-get -y upgrade
    RUN apt-get -y install openjdk-11-jdk wget
    RUN mkdir /usr/local/tomcat
    RUN wget https://dlcdn.apache.org/tomcat/tomcat-8/v8.5.78/bin/apache-tomcat-8.5.78.tar.gz  -O /tmp/tomcat.tar.gz
    RUN cd /tmp && tar xvfz tomcat.tar.gz
    RUN cp -Rv /tmp/apache-tomcat-8.5.78/* /usr/local/tomcat/
    
    RUN apt-get install sudo nano git nodejs npm -f -y
    
    RUN apt-get clean && rm -rf /var/lib/apt/lists/* 
    
    RUN mkdir -p /tmp/build
    
    WORKDIR /tmp/build/
    
    RUN git clone https://Caroli1211753@bitbucket.org/caroli1211753/devops-21-22-lmn-1211753.git
    
    WORKDIR /tmp/build/devops-21-22-lmn-1211753/CA3/part2/react-and-spring-data-rest-basic/
    
    RUN chmod u+x gradlew ==> alterações de permissões gradlew
    RUN ./gradlew clean build ==> fazer build da aplicação
    RUN cp build/libs/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/ ==> copiar ficheiro para /usr/local/tomcat/webapps/
    RUN rm -Rf /tmp/build/ ==> eliminar pasta e ficheiros /tmp/build/
    
    EXPOSE 8080 ==> posta exposta para comunicação
    CMD /usr/local/tomcat/bin/catalina.sh run ==> correr ficheiro catalina.sh

Para criar os dois containers usar o comando **docker compose up**

visitar http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/ e checkar se funciona web

visitar http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/h2-console
jdbc:h2:tcp://192.168.33.11:9092/./jpadb

para perceber se a db está a funcionar

Fazer tag da imagem e publicar no Docker Hub:

    docker login
    docker part2_tag web caroli1211753/part2_web:v1.0
    docker push caroli1211753/web:v1.0

Importante que a tag seja: *userName*/*nameImage*:*version*

Tag publicada em: https://hub.docker.com/repository/docker/caroli1211753/part2_web

Fazer tag da imagem e publicar no Docker Hub:

    docker login
    docker part2_tag db caroli1211753/part2_db:v1.0
    docker push caroli1211753/db:v1.0

Importante que a tag seja: *userName*/*nameImage*:*version*

Tag publicada em: https://hub.docker.com/repository/docker/caroli1211753/part2_db

**Volumes**
Os volumes são como pastas partilhadas entre a máquina host e o container. No caso, definimos no docker-compose que *./data:/usr/src/data-backup*, ou seja, a pasta data da máquina host partindo do directório onde está o ficheiro docker-compose comunica com a paste que está no container no caminho /usr/src/data-backup.

Assim, com o comando **docker compose exec db bash** conseguimos entrar na máquina db (semelhante ao comando ssh) e abrir a linha de comando bash.

Aqui podemos usar o comando **ls** para ver ficheiros ou pastas e conseguimos encontrar o seguinte ficheiro jpadb.mv.db que corresponde ao ficheiro que queremos guardar na máquina host.

com o comando **cp jpadb.mv.db /usr/src/data-backup** copiamos o ficheiro para a pasta /usr/src/data-backup

A razão que temos para fazer este backup é que o container corre em memória e quando é desligado perdem-se os dados. Se estiver um backup na máquina host estaremos seguros com os dados.

### Alternativa Kubernetes

Kubernetes permite colocar a aplicação a correr em containers que estão em Clusters. Kubernetes e Docker podem ser usados sozinhos mas também em conjunto.
kubernets 




pode colmatar possíveis falhas

podemos definir quantos maquinas backup



### Componentes Kubernetes

* Node and Pod ==> a unidade mais pequena dde K8's; uma abstracção de container; Normalmente temos 1 aplicação por Pod; cada Pod recebe o seu endereço de IP (para comunicar com base de dados, por exemplo, no entanto, normalmente é criado um IP permanente (service). Isto porque se o Pod ou base dados falhar não é necessário estar a colocar um novo endereço IP para restabelecer a comunicação 

* Service ==> endereço de IP permanente, mesmo que o pod vá abaixo este service mantém-se

* Ingress ==> rota dos dados para o cluster. Acesso externo à aplicação (ligado pelo Service)

* Volumes ==> armazenamento de dados (cada vez que a base de dados é re iniciada os dados perdem-se) assim, para persistir os dados são necessários os volumes para guardar os dados na máquina local ou no cluster. No entanto, o administrador é que é responsável por efectuar os backups

* Secret ==> usado para guardar dados secretos (por exemplo, password)

* ConfigMap ==> configuração externa da aplicação, como comunica, por exemplo, com a base de dados (não será necessário fazer build sempre que houver alguma falha)

* StatefulSet ==> Sabe qual o Pod que se encontra a comuicar com a persistencia de dados. Existe para aplicações sem estado ou base de dados. Não e fácil de implementar por isso normalmente as base de dados são guardadas externamente ao K8's.

* Deployment ==> é um ficheiro onde definimos como queremos o deployment (por exemplo, quantas replicas). A base de dados não pode ser replicado pelo deplyment pois pode haver inconsistência dos dados.

Vantagem se algum dos componentes for abaixo temos as replicas que definimos no Deployment que estão ligados ao mesmo service e então não haverá falhas.

#### Docker vs Kubernetes

| Parâmetros | Kubernetes | Docker  |
|:-------------  |:--------------------- |:------------|
| Desenvolvedor | Google | Docker Inc |
| Ano de lançamento | 2014 | 2013 | 
| Automatização do aumento dos containers |	Sim |   Não |  
| Configuração do cluster |	Simples, em 2 comandos |  Complicado |
| Força do cluster |	Fraco |  Forte |
| Instalação | Complexa, demora tempo | Fácil e rápido |
| Volume de dados |  volumes de armazenamento partilhado por todos os containers | volumes de armazenamento partilhado por todos os containers num único Pod |
| Escalabilidade | Lenta | Rápida |
| Quantidade de nós suportados | até 5000 | menos de 2000 |
| Limite de Containers | 300000 | 95000 |

Prós de Kubernetes:
* Open source e contumizável;

* Corre em qualquer sistema operativo;

* Organização simplificada do serviço usando Pod's;

* Confiável, foi desenvolvido pela Google;

* Ferramenta de orquestração de containers com uma grande comunidade;

* OPções de aramzenamento multipls incluindo nuvens publicas;

Contras de Kubernetes:

* Migração de sateless é comlicado;

* Funcionalidade é limitada e depende na disponibilidade da Docker API;

* Instalação e configuração complicada;

* Incompatível com Docker tools;

* O Deployment para Cluster é manual e complexo;

Prós do Docker:

* Software Open SOurce;

* Primeira configuração é simples;

* Instalação leve;

* suporta as ferramentas existentes;

* fácil de manter/guardar as versões dos contentores;

* Docker compose é de configuração simples;

* bastante documentação

* aplicação isolada

Contra do Docker:
* Sem opção para armazenamento;

* A utilização da infrastrutura básica +e complicado;

* todas as acções tem de ser feitas em comand line;

* dployment para Cluster é manual e complicado.


O Docker permite fazer o deployment mais rápido, no entanto, kubernetes é uma solução mais segura.

#### Utilizar a nossa aplicação em Kubernetes

Podemos criar um Kubernetes CLuster que consiste em dois tipos de recursos:
* Painel de Controlo ==> que coordena o cluster como (agendamento, manutenção do estado e dimensionamento das aplicações e lançamento de novas actualizações).

* Nós ==> que são trabalhadores/máquinas que correm a aplicação. Cada nó tem um Kubelet que é um agente que gere o nó e comunica com o painel de controlo. O nó também deve ter ferramentas para lidar com operações de containerização (como por exemplo o Docker). O Pod corre sempre num nó. Cada nó corre pelo menos:
  * Kubelet ==> um processo responsável pela comnicação entre painel de controlo Kubernetes e o nó;

  * E um criador de containers em execução (Docker).
    Um Cluster deve ter pelo menos 3 nós, para prever se algum nó for abaixo está sempre um em cima para continuar com a aplicação.

Para fazer o deployment da nossa aplicação usando kubectl:
Depois de termos o Cluster a funcionar, cria-se Kubernetes Deployment configuration que diz ao Kubernete como criar e actualizar as instâncias da aplicação. Este Deployment é incluido no painel de controlo para correr nós individuais no Cluster. Quando se cria um ficheiro Deployment é necessário especificar a imagem para o container a usar e número de replicas pretendidas.
Quando se cria um Deployment, o Kubernetes  cria um Pod para hospedar a instância da aplicação. Pod é uma abstração que representa um grupo de um ou mais containers (Docker) e alguns recursos partilhados desses containers (memória partilhada - volumes, rede - um IP comum pelas máquinas, informações como correr cada container - como por exemplo, imagem do container e portas a usar).
Um Pod é um conjunto de logica da aplicação (containers muito ligados), por exemplo, tem o frontend, backend e base de dados.
O Deployment cria um Pod com containers dentro.
Cada nó está ligado a um nó e mantém até ao término. No caso de falha um Pod identico é agendado.


Quando a instâncias da aplicação são criadas há um Deployment Controller que monitoriza as instâncias criadas. Se o nó com uma das instancias for abaixo o Deployment Controller altera por outra instância.
Isto permite um mecanismo de "cura própria" ou manutenção.

O comando kubectl usa a Kubernetes API para interagir com o cluster.


