
# Relatório CA5 Parte 1 - jenkins

**Nome:** Carolina Silva

**Número:** 1211753

---
## Parte 1 - Continuous integration & delivery - jenkins

### Jenkins
Jenkins é uma ferramenta de automatização open source. Ajuda na automatização das fases de desenvolvimento de software relacionadas com building, testing e deploying, facilitando a integração contínua (CI) e entrega contínua (CD). Jenkins um sistema baseado em servidores que corre em containers como por exemplo Apache Tomcat. Suporta muitos sistemas de controlo de versões como por exemplo, Git e Mercurial e pode executar Apache Ant e Maven e Gradle.
Foi feito em Java por isso tem de ter java instalado mas funciona com uma grande parte de linguagens de programação. Tem bastantes plugins que introduz funcionalidades e permitem integrar qualquer tipo de projecto.
O Jenkins permite:
* testes automáticos;

* notificação por email de uma compilação;

* relatórios;

* análise de código;

* Deployment automatizado;

* cáculo de métricas;

* manutenção do servidor

* segurança;

* distribuição das compilações e testes por várias máquinas.

A Pipeline que é seguida é definida como código (na linguagem Groovy) que normalmente é guardada no ficheiro Jenkinsfile (guardado no projecto - caso do nosso desenvolvimento de ca5, mas também pode ser criado directamente no jenkins). Há 2 tipos de sintaxes no Pipeline: Script, Declarativa (que vamos usar).
Algumas informações sobre voabulário do jenkins e pipeline:
* Job --> são o ponto central do jenkins. Definem uma pipeline completa ou apenas um etapa do processo de compilação

* Node --> programa as etapas de compilação num agenet de compilação. Cria um workspace é uma pasta (na máquina local) onde o jenkins compila o projecto.

* Stage --> agrupa etapas lógicas de compilação

* Step --> uma tarefa ou etapa de compilação

### Integração Contínua (CI) e Entrega Contínua (CD)

Integração contínua é a integração de novos desenvolvimentos no projecto várias vezes por dia para termos uma entrega/actualização de software contínua. A problemática de termos uma entrega contínua é que é necessário validar a qualidade do software para não "estragar" trabalho anterior. Quando dizemos validar a qualidade é certificarmo-nos que o software está a fazer build (que não há erros) mas também que todos os testes definidos estão ok, assim como implementação de pré-produção para testar.

A CI e CD Pipeline:
Tuno nasce de uma ideia

* Desenvolvimento

* Compilação

* Testes

* Ambiente de pré-produção

* Entrega

Chega ao produto final


### Trabalho prático CA5-part1

Para correr o jenkins directamente através do ficheiro war (como é baseado em java corre com este ficheiro na máquina local). 
Necessário transferir o ficheiro war e colocá-lo numa pasta conhecida. Na linha de comandos dentro dessa pasta correr o comando  **java -jar jenkins.war**
Nas configurações consideramos que o porto seria 8080 portanto o serviço corre agora em **localhost:8080**

Para a conclusão da Pipeline pedida, o Jenkinsfile deve conter o seguinte (comentários à frente com explicação) e ser guardado na pasta do projecto:

    pipeline {  
    agent any ==> quer dizer que vamos correr na máquina host
    
        stages { ==> dividir em fases/etapas
            stage('Checkout') { ==> nome do fase "Checkout"
                steps {
                    echo 'Checking out...' ==> echo imprime na consola o texto
                    git 'https://Caroli1211753@bitbucket.org/caroli1211753/devops-21-22-lmn-1211753' ==> vai ao repositório indicado no URL
                }
            }
            stage('Build') {
                steps {
                    echo 'Assembling...'
                    dir('CA5/part1/gradle_basic_demo') { ==> vai para a pasta indicada dentro de '' onde temos o ficheiro gradlewrapper para conseguirmos correr os comandos gradlew
                    bat './gradlew clean assemble' ==> corre os seguintes comandos: clean e assemble. No meu caso utilizo bat por utilizar máquina windows mas com MacOs deveria ser sh
                    }
                }
            }
            stage('Test') {
                        steps {
                            echo 'Testing...'
                            dir('CA5/part1/gradle_basic_demo') { ==> vai para a pasta indicada dentro de '' onde temos o ficheiro gradlewrapper para conseguirmos correr os comandos gradlew
                            bat './gradlew test' ==> corre o comando test
                            }
                        }
                    }
            stage('Archiving') {
                steps {
                    echo 'Archiving...'
                            dir('CA5/part1/gradle_basic_demo') {
                    archiveArtifacts 'build/distributions/*' ==> aquiva o que está dentro da pasta distributions
                    }
                }
            }
        }
        post {
                always {
                    junit 'CA5/part1/gradle_basic_demo/build/test-results/test/*.xml' ==> arquiva todos os ficheiros . xml que estão na pasta test
                    }
                }
    }

Para criar o Job no Jenkins:

No jenkins criar "Novo Item" do tipo "pipeline", necessário definir o nome e depois clicar "OK"

Na configuração do pipeline selccionar:
* **Definition:** "Pipeline script from SCM" --> neste caso, como vamos usar o Jenkinsfile no repositório escolhemos

* **SCM:** Git --> (Source Control Management) o que vamos usar para clonar o repositório 

* **Repository URL:** https://bitbucket.org/caroli1211753/devops-21-22-lmn-1211753/ --> endereço para o repositório a clonar

* **Credentials** no meu caso *none* porque repositório é público. Mas seria possível colocar as credentials para aceder a um repositório privado

* **Branches to build** neste caso é o master porque é onde estamos a desenvolver esta feature

* **Script Path** CA5/part1/Jenkinsfile --> caminho para o Jenkinsfile no repositório a clonar (por default está a pasta raiz do projecto).

No caso de não querermos usar o Jenkinsfile no repositório seria possível na *Definition* escolher *pipeline script* e definir direntamente no jenkins. No caso de não sabermos algum comando podemos usar o "Pipeline Syntax" para ajudar a construir o comando/step que necessitamos.

No meu caso foi necessário adicionar ao ficheiro .gitignore o seguinte comando: **!gradle-wrapper.jar** isto porque para poder usar os comandos gradlew este ficheiro tem de existir no repositório remoto para o jenkins poder clonar (o .gitignore estava a ignorar todos os ficheiros .gradle).


Sempre que se fizer alterações no Jenkinsfile fazer:
* git add .

* git commit

* git push.

Para correr o pipeline no jenkins no dashboard escolher o Pipeline criado e fazer *Build Now* e analisar os resultados

![img.png](img.png)

## Parte 2 - Continuous integration & delivery - jenkins 

Nota: Usei o mesmo código do CA5-part1 uma vez que na CA2-part2 não tinha testes.

Para o desenvolvimento deste trabalho é necessário instalar os seguintes plugins no jenkins:
* HTML Publisher --> para publicação de javadoc;

* Docker Pipeline --> para compilação facilitada do Dockerfile.

Para instalar seguir os seguintes passos:
1. Dashboard --> Gerir Jenkins --> Gerir Plugins --> Disponíveis

2. Procurar plugin e clicar "Install without restart"

3. No fim da instalação re-iniciar o jenkins.


Para podermos publicar no DockerHub é necessário de criar credentials, seguir os seguintes passos:
1. Dashboard --> Gerir Jenkins --> Manage Credentials

2. Clicar em *global*

3. Clicar em *Add Credentials*

4. Preencher:
* Kind: Username with password;

* Scope: Global

* UserName && Password: preencher com username && password

* ID: definir o ID a usar nos scripts

Necessário criar o Jenkinsfile:

    pipeline {
    agent any
    
        stages {
            stage('Checkout') {
                steps {
                    echo 'Checking out...'
                    git 'https://Caroli1211753@bitbucket.org/caroli1211753/devops-21-22-lmn-1211753'
                }
            }
            stage('Build') {
                steps {
                    echo 'Assembling...'
                    dir('CA5/part2/gradle_basic_demo') {
                    bat './gradlew clean assemble'
                    }
                }
            }
            stage('Test') {
                        steps {
                            echo 'Testing...'
                            dir('CA5/part2/gradle_basic_demo') {
                            bat './gradlew test'
                            }
                        }
                    }
            stage('Generate Jar') { 
                       steps {
                            echo 'Generating jar...'
                            dir('CA5/part2/gradle_basic_demo') {
                            bat './gradlew jar' ==> criar ficheiro.jar
                            }
                       }
                    }
            stage('Javadoc') {
                      steps {
                            echo 'Creating Javadoc...'
                            dir('CA5/part2/gradle_basic_demo') {
                            bat './gradlew javadoc' ==> criar javadoc
                            }
                       }
                    }
            stage('Archiving') {
                steps {
                    echo 'Archiving...'
                            dir('CA5/part2/gradle_basic_demo') {
                    archiveArtifacts 'build/distributions/*'
                    }
                }
            }
            stage('Docker Image') {
                        steps {
                            echo 'creating and publish...'
                            dir('CA5/part2') {
                            script {
                            docker.withRegistry('https://registry.hub.docker.com', 'dockerHub_cred') { ==> entrar com as credenciais docker
    
                            def image = docker.build("caroli1211753/ca5part2:${env.BUILD_ID}") ==> criar a imagem
                            image.push() ==> publicá-la
                                }
                            }
                            }
                        }
                    }
        }
        post {
                always {
                    dir('CA5/part2/gradle_basic_demo') {
                    junit 'build/test-results/test/*.xml'
                    }
                    publishHTML([allowMissing: false, ==> para publicar os resultados dos testes
                                alwaysLinkToLastBuild: false,
                                keepAll: false, reportDir: 'CA5/part2/gradle_basic_demo/build/docs/javadoc',
                                reportFiles: 'index.html',
                                reportName: 'HTML Report',
                                reportTitles: 'The Report'])
                                }
                    }
    }

E o dockerfile:

    FROM ubuntu:18.04
    
    RUN apt-get update -y
    RUN apt-get install openjdk-11-jdk-headless -y
    
    COPY gradle_basic_demo/build/libs/basic_demo-0.1.0.jar .
    
    EXPOSE 59001
    
    CMD java -cp basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001
    
    
    mudar dockerfile


Sempre que se fizer alterações no Jenkinsfile fazer:
* git add .

* git commit

* git push.

Para correr o pipeline no jenkins no dashboard escolher o Pipeline criado e fazer *Build Now* e analisar os resultados
![img_1.png](img_1.png)


A automatização necessita do plugin do bitbucket para fazer build em cada push e a criação de um webhook no bitbucket. No entanto, era necessário criar uma máquina virtual para termos um IP para lançarmos a comunicação entre jenkins e bitbucket.

### Alternativas a jenkins

* **CircleCi**
 
O CircleCI funciona com repositórios GitHub, GitHub Enterprise e Atlassian Bitbucket e lança compilações para cada novo commit.
O CircleCI testa automaticamente as compilações em containers do Docker ou máquinas virtuais e implanta a passagem de compilações para ambientes de destino.
O CircleCI também oferece um recurso de aprovação de fluxo de trabalho que pausa o(s) trabalho(s) até que a aprovação manual seja dada.

CircleCI suporta Go, Java, Ruby, Python, Scala, Node.js, PHP, Haskell e qualquer outra linguagem que rode em Linux ou macOS.

A empresa oferece um serviço de nuvem gerenciado com um nível gratuito disponível. O serviço de nuvem foi inicialmente escrito do zero, mas agora usa Nomad e Kubernetes da HashiCorp.

O CircleCI reduz o risco garantindo testes e lançamentos frequentes e, com o serviço de nuvem gerenciado, cuida da manutenção e do provisionamento da infraestrutura de CI. O serviço de nuvem pode ser configurado em minutos, mas é menos personalizável que o Jenkins.

Tem uma opção de experimentação grátis mas é pago. É baseada numa Cloud o que quer dizer que não é preciso um servidor dedicado.

É construido o ficheiro circleci.yml

Fácil de configurar para correr em cada push do bitbucket mas mais complicado perceber como funcionam os stages. Tentativa de configuração do ficheiro yml mas depois passe a implementação para GOCD.

* **GoCD**

GoCD é uma ferramenta de código aberto usada no desenvolvimento de software para ajudar equipas e organizações a automatizar a entrega contínua (CD) de software. Ele suporta a automatização de todo o processo de compilação-teste-liberação, desde o check-in do código até a implantação. Isso ajuda a continuar a produzir software valioso em ciclos curtos e a garantir que o software possa ser lançado de forma confiável a qualquer momento. 
GoCD suporta várias ferramentas de controle de versão, incluindo Git, Mercurial, Subversion, Perforce e TFVC (a la TFS). Outros softwares de controle de versão podem ser suportados pela instalação de plugins adicionais. GoCD é lançado sob a licença Apache 2.

Open SOurce mas necessário intalar server e agent.
Tentativa de implementação.
Necessário instalar GOServer (corre no porto 8153) e GOAgent https://www.gocd.org/download/#windows

No Server é possível criar a pipeline
Dashboard --> New Pipeline --> New job --> Add stage

Assemble
![img_5.png](img_5.png)

![img_4.png](img_4.png)

Test

![img_6.png](img_6.png)

![img_7.png](img_7.png)

![img_8.png](img_8.png)

Generate Jar

![img_9.png](img_9.png)

javadoc

![img_10.png](img_10.png)

Dockerfile

![img_11.png](img_11.png)

![img_12.png](img_12.png)


Estão consideradas as seguintes stages:

![img_2.png](img_2.png)

![img_3.png](img_3.png)

A fase de ligaçao e publicação da imagem no Docker não foi possível. APesar de ter instaldado um plugin não consegui.

Pareceu-me mais amigável ao utilizador mas a forma como implementei perde-se mais tempo a configurar cada stage. No entanto é mais visível o que se está a fazer por causa da interface visual.

Aqui consegui que corra os builds ao após cada commit.


Entendo que esta não é a forma de tratar infrastructure as code para isso teria de usar o ficheiro ca5part2alternative1.gocd.yaml e configurar cada stage.


* **Bitbucket pipeline**


Integrado no bitbucket e os steps podem ser configurados no ficheiro bitbucket-pipeline.yml

Escolhendo uma versão que tenha de exemplo é simples, no entanto, quando se sai do padrão é mais complicado configurar.
Vantagem será velocidade, no entanto a comparação é difícil porque é contabilizado em minutos. 

Nota: Tentei implementação nestes 3 mas o que avançou melhor foi GoCD.  